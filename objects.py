class matrix:
    def __init__(self, lines:list):
        self.lines = lines
        self.nb_lines = len(lines)
        self.nb_columns = len(lines[0])

    def print(self):
        for line in self.lines:
            print(line)
        print()

def matrix_sum(m1:matrix,m2:matrix):
    if m1.nb_lines == m2.nb_lines and m1.nb_columns == m2.nb_columns:
        result = []
        for l1,l2 in zip(m1.lines,m2.lines):
            result.append([i+j for i,j in zip(l1,l2)])
        return matrix(result)
    
def product_by_scal(scal:float,m:matrix):
    result = []
    for l in m.lines:
        result.append([scal*a for a in l])
    return matrix(result)